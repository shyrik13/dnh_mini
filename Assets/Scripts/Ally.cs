﻿using UnityEngine;
using System.Collections;

public class Ally : MonoBehaviour {

    public int Speed;
    public Transform player;
    private float minDist = 2;
    private float maxDist = 15;


    void Start()
    {
    }

    void Update()
    {
        if (Vector3.Distance(transform.position, player.position) >= minDist & Vector3.Distance(transform.position, player.position) <= maxDist)
        {
            transform.position += transform.forward * Speed * Time.deltaTime;
            transform.LookAt(player);
        }
    }
}

       
    
