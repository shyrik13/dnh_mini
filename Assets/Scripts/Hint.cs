﻿using UnityEngine;
using System.Collections;

public class Hint : MonoBehaviour {

    public Transform hint;
    public Transform omen;
    public Transform player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.E) & Vector3.Distance(transform.position,player.transform.position) <= 3)
        {
            hint.gameObject.SetActive(true);
            omen.gameObject.SetActive(false);
            player.GetComponent<Movement>().enabled = false;
        }
	}
    public void Ingame()
    {
        hint.gameObject.SetActive(false);
        player.GetComponent<Movement>().enabled = true;
    }
}
