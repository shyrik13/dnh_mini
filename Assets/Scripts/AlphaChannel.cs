﻿using UnityEngine;
using System.Collections;

public class AlphaChannel : MonoBehaviour
{

    public Transform obj;
    public Transform objAlpha;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            obj.gameObject.SetActive(false);
            objAlpha.gameObject.SetActive(true);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            obj.gameObject.SetActive(true);
            objAlpha.gameObject.SetActive(false);
        }
    }
}