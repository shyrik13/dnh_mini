﻿using UnityEngine;
using System.Collections;

public class DNHenter : MonoBehaviour {
    public Transform dnh1;
    public Transform dnh2;
    public Transform stairs;
    public Transform roof;

	// Use this for initialization
	void Start () {
        dnh1.gameObject.SetActive(true);
        dnh2.gameObject.SetActive(true);
        stairs.gameObject.SetActive(true);
        roof.gameObject.SetActive(true);
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            dnh1.gameObject.SetActive(true);
            dnh2.gameObject.SetActive(false);
            stairs.gameObject.SetActive(true);
            roof.gameObject.SetActive(false);
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            dnh1.gameObject.SetActive(true);
            dnh2.gameObject.SetActive(true);
            stairs.gameObject.SetActive(true);
            roof.gameObject.SetActive(true);
        }
    }
}
