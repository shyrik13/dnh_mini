﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

    public float speed = 1.5f;
    public Transform up, down,  left, right;
    
    void Start()
    {
        //Screen.lockCursor = true;
        //Cursor.visible = false;
    }
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
            up.gameObject.SetActive(false);
            down.gameObject.SetActive(false);
            left.gameObject.SetActive(true);
            right.gameObject.SetActive(false);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
            up.gameObject.SetActive(false);
            down.gameObject.SetActive(false);
            left.gameObject.SetActive(false);
            right.gameObject.SetActive(true);
        }
        if (Input.GetKey(KeyCode.W))
        {
            transform.position += Vector3.forward * speed * Time.deltaTime;
            up.gameObject.SetActive(true);
            down.gameObject.SetActive(false);
            left.gameObject.SetActive(false);
            right.gameObject.SetActive(false);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.position += Vector3.back * speed * Time.deltaTime;
            up.gameObject.SetActive(false);
            down.gameObject.SetActive(true);
            left.gameObject.SetActive(false);
            right.gameObject.SetActive(false);
        }
    }
}

